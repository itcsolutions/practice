package ru.itcsolutions.example.repository;

import org.springframework.stereotype.Service;
import ru.itcsolutions.example.repository.model.BookOrder;
import ru.itcsolutions.example.repository.model.DressOrder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Пример - эммуляция бд
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
@Service
public class ExampleOrdersRepository {

    private List<ExampleOrder> orders;

    /*
    Конструктор
    Тут создаются на старте приложения некие начальные значения заказов
     */
    public ExampleOrdersRepository() {
        orders = new ArrayList<>();

        orders.add(new BookOrder(1L, "Сильмариллион", LocalDateTime.now(), 100, true));
        orders.add(new BookOrder(2L, "Основы паттернов программирования", LocalDateTime.now(), 59, false));
        orders.add(new DressOrder(3L, "Костюм динозавра", LocalDateTime.now(), 999, false));
        orders.add(new BookOrder(4L, "50 оттенков серого", LocalDateTime.now(), 58, true));
        orders.add(new DressOrder(5L, "Стринги", LocalDateTime.now(), 5, true));

    }

    /**
     * Вернуть список целиком
     * @return список
     */
    public List<ExampleOrder> getOrders() {
        return orders;
    }

    /**
     * Обновить список
     * @param orders - список закзаов
     */
    public void setOrders(List<ExampleOrder> orders) {
        this.orders = orders;
    }
}
